const getPosition = () => {
  const { getCurrentPosition } = navigator.geolocation;

  const options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };

  return new Promise((resolve, reject) =>
    getCurrentPosition(resolve, reject, options)
  );
};

export default getPosition;
