import React from 'react';
import { StyleSheet, Text, View, Button, AsyncStorage } from 'react-native';
import axios from 'axios';
import apiKey from './apiKey';

// Helpers
import getPosition from './helpers/getPosition';

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      temperature: 0,
      humidity: 0,
      time: 0
    };
  }

  componentDidMount() {
    this.loadData();
  }

  loadData = async () => {
    try {
      const data = await AsyncStorage.getItem('weatherData');
      if (data) {
        const { temperature, humidity } = JSON.parse(data);
        this.setData({ temperature, humidity });
      }
      await this.refreshCurrentWeather();
    } catch (error) {
      console.warn('There was a problem restoring the data...');
    }
  };

  setData = ({ temperature, humidity }, cb = () => {}) => {
    const data = {
      temperature,
      humidity,
      time: this.getCurrentTime()
    };
    this.setState(data);
    cb(data);
  };

  cacheData = async data => {
    try {
      await AsyncStorage.setItem('weatherData', JSON.stringify(data));
    } catch (error) {
      console.warn('There was a problem saving the data...');
    }
  };

  getCurrentTime = () => {
    const date = new Date();
    return `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
  };

  getCurrentWeather = async (latitude, longitude) => {
    try {
      const apiURL = `http://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&units=metric&appid=${apiKey}`;
      const { data } = await axios({
        url: apiURL,
        method: 'get',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      });
      const { temp: temperature, humidity } = data.main;
      return { temperature, humidity };
    } catch (error) {
      console.warn('There was a problem getting the weather data...');
    }
  };

  refreshCurrentWeather = async () => {
    const { coords: { latitude, longitude } } = await getPosition();
    const { temperature, humidity } = await this.getCurrentWeather(
      latitude,
      longitude
    );
    this.setData({ temperature, humidity }, this.cacheData);
  };

  render() {
    const { temperature, humidity, time } = this.state;

    return (
      <View style={styles.container}>
        <Text style={styles.title}>Weather At Your Location</Text>
        <View style={styles.content}>
          <Text style={styles.text}>Temperature: {temperature} ℃</Text>
          <Text style={styles.text}>Humidity: {humidity}%</Text>
          <Text style={styles.text}>Last Request: {time}</Text>
        </View>
        <Button
          onPress={this.refreshCurrentWeather}
          title="Refresh"
          accessibilityLabel="Refresh the weather!"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 50
  },
  title: {
    fontSize: 30,
    marginBottom: 50
  },
  content: {
    marginBottom: 50
  },
  text: {
    fontSize: 18
  }
});
